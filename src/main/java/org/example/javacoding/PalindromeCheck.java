package org.example.javacoding;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PalindromeCheck {

    private Logger LOG = LoggerFactory.getLogger(PalindromeCheck.class);

    public boolean checkIfIntegerNumberIsPalindrome(int testNumber) {
        if (testNumber < 0) {
            LOG.info("Number is negative");
            return false;
        }

        return checkIfStringIsPalindrome(String.valueOf(testNumber));
    }

    public boolean checkIfDecimalNumberIsPalindrome(float testDecimalNumber) {
        if (testDecimalNumber < 0) {
            LOG.info("Decimal is negative");
            return false;
        }

        String[] decimalNumberArray = String.valueOf(testDecimalNumber).split("\\.");

        if (decimalNumberArray[0].length() != decimalNumberArray[1].length()) {
            LOG.info("Number of digits before and after decimal are not equal");
            return false;
        }

        return checkIfStringIsPalindrome(String.join("", decimalNumberArray));
    }

    public boolean checkIfStringIsPalindrome(String testString) {
        if (testString == null) {
            throw new NullPointerException("String is null");
        }

        if (testString.length() == 0 || testString.length() == 1) {
            LOG.info("String is single character");
            return true;
        }

        int start = 0;
        int end  = testString.length() - 1;

        while (start < end) {
            if (testString.charAt(start) != testString.charAt(end)) {
                LOG.info("Character not equal for string '" + testString + "' at indexes : " + start + " " + end);
                return false;
            }

            start++;
            end--;
        }
        LOG.info("String is palindrome : " + testString);
        return true;
    }

    public boolean checkIfPhraseIsPalindrome(String testPhrase) {
        testPhrase = testPhrase.replaceAll("[\\p{Punct}\s]", "");
        return checkIfStringIsPalindrome(testPhrase.toLowerCase());
    }
}
