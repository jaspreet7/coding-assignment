package org.example.playstore;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayDeque;
import java.util.Queue;

public class Playlist {

    @Getter
    private Queue<Song> playList;
    @Getter
    private int initialCapacity;
    @Getter @Setter
    private String playListName;

    public Playlist(int initialCapacity, String playListName) {
        this.playListName = playListName;
        this.initialCapacity = initialCapacity;
        playList = new ArrayDeque<>(initialCapacity);
    }

    public void addSongToPlaylist(Song song) {
        if (playList.size() == initialCapacity) {
            playList.poll();
        }
        playList.offer(song);
    }
}
