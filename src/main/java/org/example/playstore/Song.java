package org.example.playstore;

import lombok.*;

public class Song {

    @Getter
    private long songId;
    @Getter
    private String songName;

    public Song(String songName) {
        this.songName = songName;
        this.songId = System.currentTimeMillis();
    }
}
