package org.example.playstore;


import lombok.Getter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.lang.model.type.MirroredTypeException;
import java.util.*;
import java.util.stream.Collectors;

public class Store {

    private Logger LOG = LoggerFactory.getLogger(Store.class);
    private static Store instance;
    @Getter
    private Map<Long, Playlist> userPlaylistMap;
    @Getter
    private List<Song> songList;

    private Store() {
        userPlaylistMap = new HashMap<>();
        songList = new ArrayList<>();
    }

    public static Store getInstance() {
        if (instance == null) {
            instance = new Store();
        }
        return instance;
    }

    public boolean addSongToStore(String songName) {
        Song song = getSongFromName(songName);
        if (song != null) {
            LOG.info("Song already present in store");
            return false;
        }
        song = new Song(songName);
        songList.add(song);
        LOG.info("Song added successfully");
        return true;
    }

    public List<String> getAllSongs() {
        return songList.stream().map(Song::getSongName).collect(Collectors.toList());
    }

    public synchronized long userSignUpWithPlaylist(String userName, int initialCapacity, String playListName) {
        User user = new User(userName);
        Playlist playlist = new Playlist(initialCapacity, playListName);
        userPlaylistMap.put(user.getUserId(), playlist);
        return user.getUserId();
    }

    public synchronized void playSong(long userId, String songName) {
        Playlist playlist = userPlaylistMap.get(userId);
        if (playlist == null) {
            throw new IllegalArgumentException("User signUp not yet done for userId : " + userId);
        }
        Song song = getSongFromName(songName);
        if (song != null) {
            playlist.addSongToPlaylist(song);
        } else {
            throw new IllegalArgumentException("Requested Song '" + songName + "' not present in store");
        }
    }

    private Song getSongFromName(String songName) {
        for (Song s : songList) {
            if (s.getSongName().equals(songName))
                return s;
        }
        return null;
    }
}
