package org.example.playstore;

import lombok.*;

public class User {

    @Getter
    private long userId;
    @Getter @Setter
    private String userName;

    public User(String userName) {
        this.userName = userName;
        this.userId = System.currentTimeMillis();
    }
}
