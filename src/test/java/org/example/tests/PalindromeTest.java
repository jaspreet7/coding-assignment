package org.example.tests;

import org.example.javacoding.PalindromeCheck;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

@Tag("PalindromeTest")
public class PalindromeTest {
    PalindromeCheck palindromeCheck;

    @BeforeEach
    public void setUpObject() {
        palindromeCheck = new PalindromeCheck();
    }

    private static Stream<Arguments> testStringIsPalindromeData() {
        return Stream.of(
                Arguments.of("", true),
                Arguments.of("gekeg", true),
                Arguments.of("hello", false),
                Arguments.of("%66*&*66%", true),
                Arguments.of("c", true)
        );
    }

    private static Stream<Arguments> testNumberIsPalindromeData() {
        return Stream.of(
                Arguments.of(-1, false),
                Arguments.of(0, true),
                Arguments.of(141, true),
                Arguments.of(1341, false)
        );
    }

    private static Stream<Arguments> testDecimalIsPalindromeData() {
        return Stream.of(
                Arguments.of(-13.31f, false),
                Arguments.of(13.31f, true),
                Arguments.of(1.21f, false)
        );
    }

    @ParameterizedTest
    @MethodSource("testStringIsPalindromeData")
    public void verifyStringIsPalindrome(String testString, boolean expected) {
        //When
        boolean result = palindromeCheck.checkIfStringIsPalindrome(testString);

        //Assert
        Assertions.assertEquals(expected, result, "String should be palindrome - " + expected);
    }

    @ParameterizedTest
    @MethodSource("testNumberIsPalindromeData")
    public void verifyNumberIsPalindrome(int testNumber, boolean expected) {
        //When
        boolean result = palindromeCheck.checkIfIntegerNumberIsPalindrome(testNumber);

        //Assert
        Assertions.assertEquals(expected, result, "Number should be palindrome - " + expected);
    }

    @ParameterizedTest
    @MethodSource("testDecimalIsPalindromeData")
    public void verifyDecimalIsPalindrome(float testNumber, boolean expected) {
        //When
        boolean result = palindromeCheck.checkIfDecimalNumberIsPalindrome(testNumber);

        //Assert
        Assertions.assertEquals(expected, result, "Decimal should be palindrome - " + expected);
    }


    @Test
    public void verifyNullPointerException() {
        //Assert
        Assertions.assertThrows(NullPointerException.class, () -> palindromeCheck.checkIfStringIsPalindrome(null));
    }

    @Test
    public void verifyPhraseIsPalindrome() {
        //When
        boolean result = palindromeCheck.checkIfPhraseIsPalindrome("Don't nod");

        //Assert
        Assertions.assertEquals(true, result, "Phrase should be palindrome - " + true);
    }
}
