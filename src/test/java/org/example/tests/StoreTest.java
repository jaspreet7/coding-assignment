package org.example.tests;

import org.example.playstore.Playlist;
import org.example.playstore.Song;
import org.example.playstore.Store;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

@Tag("StoreTest")
public class StoreTest {

    private Logger LOG = LoggerFactory.getLogger(StoreTest.class);
    private static Store store;
    private static final String NEW_SONG_NAME = "NS";
    private static final String EXISTING_SONG_NAME = "S1";
    private static final String ABSENT_SONG_NAME = "AS";
    private static final boolean IS_NEW_SONG = true;
    private static final boolean IS_EXISTING_SONG = false;
    private static final boolean IS_VALID_USER = true;
    private static final boolean IS_INVALID_USER = false;

    @BeforeAll
    public static void setup() {
        store = Store.getInstance();
        Song s1 = new Song("S1");
        Song s2 = new Song("S2");
        Song s3 = new Song("S3");
        Song s4 = new Song("S4");
        Song s5 = new Song("S5");
        Song s6 = new Song("S6");

        store.addSongToStore(s1.getSongName());
        store.addSongToStore(s2.getSongName());
        store.addSongToStore(s3.getSongName());
        store.addSongToStore(s4.getSongName());
        store.addSongToStore(s5.getSongName());
        store.addSongToStore(s6.getSongName());
    }

    private static Stream<Arguments> addSongToStoreData() {
        return Stream.of(
          Arguments.of(NEW_SONG_NAME, IS_NEW_SONG),
          Arguments.of(EXISTING_SONG_NAME, IS_EXISTING_SONG)
        );
    }

    private static Stream<Arguments> playSongInvalidData() {
        return Stream.of(
                Arguments.of(IS_VALID_USER, ABSENT_SONG_NAME),
                Arguments.of(IS_INVALID_USER, EXISTING_SONG_NAME)
        );
    }

    private static Stream<Arguments> playSongValidData() {
        return Stream.of(
                Arguments.of(Arrays.asList("S1", "S2", "S3", "S4", "S5"), "User1" ,3, "PL1"),
                Arguments.of(Arrays.asList("S1", "S2", "S3", "S4", "S5", "S6"), "User2", 4, "PL2")
        );
    }

    @ParameterizedTest(name = "Test add song to store for new and existing song")
    @MethodSource("addSongToStoreData")
    public void testAddSongToStore(String songName, boolean newlyAdded) {
        boolean songAdded = store.addSongToStore(songName);
        Assertions.assertEquals(newlyAdded, songAdded, "Song '" + songName + "' existence in store is incorrect");
        LOG.info("Success testAddSongToStore");
    }

    @ParameterizedTest(name = "Test add song to store for new and existing song")
    @MethodSource("playSongInvalidData")
    public void testPlaySongExceptions(boolean isUserValid, String songName) {
        long userId = 0;
        if (isUserValid) {
            userId = store.userSignUpWithPlaylist("New User", 3, "PLValid");
        }
        long finalUserId = userId;
        Assertions.assertThrows(IllegalArgumentException.class, () -> store.playSong(finalUserId, songName));
        LOG.info("Success playSongInvalidData");
    }

    @ParameterizedTest(name = "Test add song to store for new and existing song")
    @MethodSource("playSongValidData")
    public void testPlaySongValidData(List<String> songList, String userName, int initCap, String playlistName) {
        long userId = store.userSignUpWithPlaylist(userName, initCap, playlistName);
        for (int i=0; i<initCap; i++) {
            store.playSong(userId, songList.get(i));
        }
        Playlist playlist = store.getUserPlaylistMap().get(userId);

        //Asserting initial playlist creation till init cap
        Assertions.assertEquals(playlist.getPlayListName(), playlistName, "Playlist name is incorrect");
        Assertions.assertTrue(playlist.getPlayList().size() >= initCap, "Songs not added correctly in playlist");
        Assertions.assertEquals( songList.get(0), playlist.getPlayList().peek().getSongName(), "Song name is incorrect");

        //Asserting initial playlist update after init cap
        int count = initCap;
        int pos = 1;
        while (count < songList.size()) {
            store.playSong(userId, songList.get(count));
            playlist = store.getUserPlaylistMap().get(userId);
            Assertions.assertEquals(songList.get(pos), playlist.getPlayList().peek().getSongName(), "Song name is incorrect");
            count++;
            pos++;
        }
        LOG.info("Success playSongValidData");
    }
}
